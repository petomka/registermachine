import me.petomka.registermachine.model.commands.Arguments;
import static org.junit.Assert.*;
import org.junit.Test;

public class ArgumentsTest {

	@Test
	public void testArguments() {
		Arguments arguments = new Arguments("1 2 3..4 and 5<=6");
		assertEquals(1, arguments.getInt(0));
		assertEquals(2, arguments.getInt(1));
		assertEquals(3, arguments.getInt(2));
		assertEquals(4, arguments.getInt(3));
		assertEquals(5, arguments.getInt(4));
		assertEquals(6, arguments.getInt(5));

		assertEquals("..", arguments.getArg(0));
		assertEquals("and", arguments.getArg(1));
		assertEquals("<=", arguments.getArg(2));

		arguments = new Arguments("if 1<0 goto 5");
		assertEquals("if", arguments.getArg(0));
		assertEquals(1, arguments.getInt(0));
		assertEquals("<", arguments.getArg(1));
		assertEquals(0, arguments.getInt(1));
		assertEquals("goto", arguments.getArg(2));
		assertEquals(5, arguments.getInt(2));
	}

	@Test
	public void testEndArgument() {
		Arguments arguments = new Arguments("end");
		assertEquals("end", arguments.getArg(0));
	}

}
