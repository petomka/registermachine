package me.petomka.registermachine;

import me.petomka.registermachine.model.RegisterMachine;

public class RegisterMachineMain {

	public static void main(String[] args) {
		RegisterMachine machine = new RegisterMachine();

		//The below program will compute c(3) = 13 * c(1) * c(2) without the use of the multiplication commands

		machine.getMemory().getRegister(1).setValue(5);
		machine.getMemory().getRegister(2).setValue(6);

		machine.compileProgram(new String[]{
				"load 1",
				"store 4",
				"goto 10", //3
				"load 3", //4
				"add 2",
				"store 3",
				"load 4",
				"c-sub 1",
				"store 4",
				"if 4>0 goto 4", //5
				"load 3",
				"store 1",
				"c-load 0",
				"store 3",
				"load 1",
				"store 4",
				"goto 24", //17
				"load 3",
				"c-add 13",
				"store 3",
				"load 4",
				"c-sub 1",
				"store 4",
				"if 4>0 goto 18",
				"end"
		});

		while (!machine.isTerminated()) {
			machine.step();
		}

		System.out.println(machine.memDump(10));
	}

}
