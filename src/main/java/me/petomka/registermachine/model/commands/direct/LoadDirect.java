package me.petomka.registermachine.model.commands.direct;

import me.petomka.registermachine.model.MachineException;
import me.petomka.registermachine.model.Register;
import me.petomka.registermachine.model.RegisterMachine;
import me.petomka.registermachine.model.commands.Command;

public class LoadDirect extends Command {

	public LoadDirect(int targetRegister) throws MachineException {
		super(targetRegister);
		if(targetRegister == 0) {
			throw new MachineException("Cannot load from accumulator");
		}
	}

	@Override
	public void execute(RegisterMachine registerMachine) throws MachineException {
		Register register = getTargetRegister(registerMachine);
		int value = register.getValue();
		registerMachine.getAccumulator().setValue(value);

		registerMachine.getProgramCounter().increment();
	}
}
