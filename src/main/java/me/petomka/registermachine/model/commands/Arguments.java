package me.petomka.registermachine.model.commands;

import me.petomka.registermachine.model.MachineException;

import java.util.ArrayList;
import java.util.List;

public class Arguments {

	private final String commandLine;

	private List<Integer> argInts = new ArrayList<>();

	private List<String> argStrings = new ArrayList<>();

	public Arguments(String commandLine) {
		this.commandLine = commandLine;

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < commandLine.length(); i++) {
			char c = commandLine.charAt(i);

			if (c < '0' || c > '9') {
				sb.append(c);
				continue;
			}

			String arg = sb.toString();
			arg = arg.trim();
			if (!arg.isEmpty()) {
				argStrings.add(arg);
				sb = new StringBuilder();
			}

			Integer parsed = parseInt(i);
			argInts.add(parsed);
		}

		String arg = sb.toString();
		arg = arg.trim();
		if (!arg.isEmpty()) {
			argStrings.add(arg);
		}
	}

	public int getInt(int index) throws MachineException {
		return argInts.get(index);
	}

	public String getArg(int index) {
		return argStrings.get(index);
	}

	private Integer parseInt(int startIndex) {
		char c = commandLine.charAt(startIndex);

		Integer result = null;

		while (c >= '0' && c <= '9') {
			if (result == null) {
				result = 0;
			}
			result *= 10;
			result += c - '0';

			if (startIndex + 1 >= commandLine.length()) {
				break;
			}

			c = commandLine.charAt(++startIndex);
		}

		return result;
	}

}
