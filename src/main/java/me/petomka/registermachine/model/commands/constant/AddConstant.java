package me.petomka.registermachine.model.commands.constant;

import me.petomka.registermachine.model.MachineException;
import me.petomka.registermachine.model.Register;
import me.petomka.registermachine.model.RegisterMachine;
import me.petomka.registermachine.model.commands.Command;

public class AddConstant extends Command {

	public AddConstant(int targetValue) throws MachineException {
		super(targetValue);
	}

	@Override
	public void execute(RegisterMachine registerMachine) throws MachineException {
		Register acc = registerMachine.getAccumulator();
		int result = acc.getValue() + getTargetValue();
		acc.setValue(result);

		registerMachine.getProgramCounter().increment();
	}
}
