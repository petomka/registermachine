package me.petomka.registermachine.model.commands.direct;

import me.petomka.registermachine.model.MachineException;
import me.petomka.registermachine.model.Register;
import me.petomka.registermachine.model.RegisterMachine;
import me.petomka.registermachine.model.commands.Command;

public class StoreDirect extends Command {

	public StoreDirect(int targetRegister) throws MachineException {
		super(targetRegister);
		if(targetRegister == 0) {
			throw new MachineException("Cannot store to accumulator");
		}
	}

	@Override
	public void execute(RegisterMachine registerMachine) throws MachineException {
		Register register = getTargetRegister(registerMachine);
		register.setValue(registerMachine.getAccumulator().getValue());

		registerMachine.getProgramCounter().increment();
	}
}
