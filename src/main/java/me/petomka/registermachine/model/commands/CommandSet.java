package me.petomka.registermachine.model.commands;

import me.petomka.registermachine.model.MachineException;
import me.petomka.registermachine.model.commands.constant.AddConstant;
import me.petomka.registermachine.model.commands.constant.DivConstant;
import me.petomka.registermachine.model.commands.constant.LoadConstant;
import me.petomka.registermachine.model.commands.constant.MultConstant;
import me.petomka.registermachine.model.commands.constant.SubConstant;
import me.petomka.registermachine.model.commands.direct.*;
import me.petomka.registermachine.model.commands.indirect.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class CommandSet {

	private interface UnsafeBiFunction<T, U, R> {

		R apply(T t, U u) throws Exception;

	}

	private final Map<String, Class<? extends Command>> commandMap = new HashMap<>();
	private final Map<String, UnsafeBiFunction<Arguments, Class<? extends Command>, Command>> commandSuppliers =
			new HashMap<>();

	public CommandSet() {
		registerSingleIntCommand("load", LoadDirect.class);
		registerSingleIntCommand("store", StoreDirect.class);
		registerSingleIntCommand("add", AddDirect.class);
		registerSingleIntCommand("sub", SubDirect.class);
		registerSingleIntCommand("mult", MultDirect.class);
		registerSingleIntCommand("div", DivDirect.class);
		registerSingleIntCommand("goto", GotoDirect.class);
		registerConditionalJumpCommand("if", ConditionalJumpCommand.class);
		registerNoArgCommand("end", EndCommand.class);

		//commands with constants
		registerSingleIntCommand("c-load", LoadConstant.class);
		registerSingleIntCommand("c-add", AddConstant.class);
		registerSingleIntCommand("c-sub", SubConstant.class);
		registerSingleIntCommand("c-mult", MultConstant.class);
		registerSingleIntCommand("c-div", DivConstant.class);

		//indirect commands
		registerSingleIntCommand("ind-load", LoadIndirect.class);
		registerSingleIntCommand("ind-store", StoreIndirect.class);
		registerSingleIntCommand("ind-add", AddIndirect.class);
		registerSingleIntCommand("ind-sub", SubIndirect.class);
		registerSingleIntCommand("ind-mult", MultIndirect.class);
		registerSingleIntCommand("ind-div", DivIndirect.class);
	}

	private void registerSingleIntCommand(String name, Class<? extends Command> commandClass) {
		commandMap.put(name, commandClass);
		commandSuppliers.put(name, this::singleIntCommand);
	}

	private void registerNoArgCommand(String name, Class<? extends Command> commandClass) {
		commandMap.put(name, commandClass);
		commandSuppliers.put(name, this::noArgCommand);
	}

	private void registerConditionalJumpCommand(String name, Class<? extends Command> commandClass) {
		commandMap.put(name, commandClass);
		commandSuppliers.put(name, this::conditionalJumpCommand);
	}

	private Command noArgCommand(Arguments arguments, Class<? extends Command> commandClass) throws Exception {
		Constructor<? extends Command> constructor = commandClass.getConstructor(int.class);
		return constructor.newInstance(0);
	}

	private Command singleIntCommand(Arguments arguments, Class<? extends Command> commandClass) throws Exception {
		int value = arguments.getInt(0);
		Constructor<? extends Command> constructor = commandClass.getConstructor(int.class);
		return constructor.newInstance(value);
	}

	private Command conditionalJumpCommand(Arguments arguments,
										   Class<? extends Command> commandClass) throws Exception {
		int register = arguments.getInt(0);
		String operator = arguments.getArg(1); //Arg 0 is command.
		int value = arguments.getInt(1);
		String GOTO = arguments.getArg(2);

		if (!GOTO.equalsIgnoreCase("goto")) {
			throw new MachineException("Invalid syntax for if command");
		}

		int pc = arguments.getInt(2);

		Constructor<? extends Command> constructor =
				commandClass.getConstructor(int.class, int.class, String.class, int.class);

		return constructor.newInstance(register, value, operator, pc);
	}

	public Command loadCommand(String line) throws MachineException {
		Arguments arguments = new Arguments(line);

		String cmdName = arguments.getArg(0);
		cmdName = cmdName.toLowerCase();

		Class<? extends Command> commandClass = commandMap.get(cmdName);
		UnsafeBiFunction<Arguments, Class<? extends Command>, Command> function =
				commandSuppliers.get(cmdName);

		if (commandClass == null || function == null) {
			throw new MachineException("Unknown command: " + line);
		}

		try {
			return function.apply(arguments, commandClass);
		} catch (Exception e) {
			String msg = e.getMessage();
			if(e instanceof InvocationTargetException) {
				msg = ((InvocationTargetException) e).getTargetException().getMessage();
			}
			throw new MachineException("Error parsing command: \n" + msg);
		}
	}

}
