package me.petomka.registermachine.model.commands.direct;

import me.petomka.registermachine.model.MachineException;
import me.petomka.registermachine.model.RegisterMachine;
import me.petomka.registermachine.model.commands.Command;

public class EndCommand extends Command {

	public EndCommand(int targetValue) throws MachineException {
		super(targetValue);
	}

	@Override
	public void execute(RegisterMachine registerMachine) throws MachineException {
		registerMachine.end();
	}
}
