package me.petomka.registermachine.model.commands.indirect;

import me.petomka.registermachine.model.MachineException;
import me.petomka.registermachine.model.Register;
import me.petomka.registermachine.model.RegisterMachine;
import me.petomka.registermachine.model.commands.Command;

public class StoreIndirect extends Command {

	public StoreIndirect(int targetValue) throws MachineException {
		super(targetValue);
	}

	@Override
	public void execute(RegisterMachine registerMachine) throws MachineException {
		Register register = getTargetRegisterIndirect(registerMachine);
		Register acc = registerMachine.getAccumulator();

		register.setValue(acc.getValue());

		registerMachine.getProgramCounter().increment();
	}
}
