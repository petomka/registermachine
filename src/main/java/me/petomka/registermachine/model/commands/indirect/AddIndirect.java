package me.petomka.registermachine.model.commands.indirect;

import me.petomka.registermachine.model.MachineException;
import me.petomka.registermachine.model.Register;
import me.petomka.registermachine.model.RegisterMachine;
import me.petomka.registermachine.model.commands.Command;

public class AddIndirect extends Command {

	public AddIndirect(int targetValue) throws MachineException {
		super(targetValue);
	}

	@Override
	public void execute(RegisterMachine registerMachine) throws MachineException {
		Register register = getTargetRegisterIndirect(registerMachine);
		Register acc = registerMachine.getAccumulator();

		int result = acc.getValue()+ register.getValue();
		acc.setValue(result);

		registerMachine.getProgramCounter().increment();
	}
}
