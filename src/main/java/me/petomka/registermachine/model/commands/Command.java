package me.petomka.registermachine.model.commands;

import lombok.Getter;
import me.petomka.registermachine.model.MachineException;
import me.petomka.registermachine.model.Register;
import me.petomka.registermachine.model.RegisterMachine;

@Getter
public abstract class Command {

	private final int targetValue;

	public Command(int targetValue) throws MachineException {
		if (targetValue < 0) {
			throw new MachineException("Command value must be greater than or equal to zero! Was: " + targetValue);
		}
		this.targetValue = targetValue;
	}

	protected Register getTargetRegister(RegisterMachine registerMachine) throws MachineException {
		if(targetValue == 0) {
			return registerMachine.getAccumulator();
		}
		return registerMachine.getMemory().getRegister(targetValue);
	}

	protected Register getTargetRegisterIndirect(RegisterMachine registerMachine) throws MachineException {
		Register register = getTargetRegister(registerMachine);
		return registerMachine.getMemory().getRegister(register.getValue());
	}

	/**
	 * Executes the command in context of the given register machine.
	 * Also increments or sets the program counter at the end of the command routine.
	 * @param registerMachine the register machine in the context
	 * @throws MachineException if execution fails
	 */
	public abstract void execute(RegisterMachine registerMachine) throws MachineException;

}
