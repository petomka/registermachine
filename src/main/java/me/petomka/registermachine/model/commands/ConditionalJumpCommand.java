package me.petomka.registermachine.model.commands;

import me.petomka.registermachine.model.MachineException;
import me.petomka.registermachine.model.Register;
import me.petomka.registermachine.model.RegisterMachine;

import java.util.HashMap;
import java.util.Map;

public class ConditionalJumpCommand extends Command {

	private interface IntBiPredicate {

		boolean test(int a, int b);

	}

	private static final Map<String, IntBiPredicate> OPERATORS = new HashMap<>();

	static {
		OPERATORS.put("=", (a, b) -> a == b);
		OPERATORS.put("<=", (a, b) -> a <= b);
		OPERATORS.put(">=", (a, b) -> a >= b);
		OPERATORS.put("<", (a, b) -> a < b);
		OPERATORS.put(">", (a, b) -> a > b);
	}

	private final IntBiPredicate op;

	private final int value;

	private final int programCounterValue;

	public ConditionalJumpCommand(int targetRegister, int value, String operator, int programCounter) throws MachineException {
		super(targetRegister);

		this.value = value;
		this.programCounterValue = programCounter;

		op = OPERATORS.get(operator);
		if(op == null) {
			throw new MachineException("Invalid operator for jump command: " + operator);
		}
	}

	@Override
	public void execute(RegisterMachine registerMachine) throws MachineException {
		Register register = getTargetRegister(registerMachine);

		if(!op.test(register.getValue(), value)) {
			registerMachine.getProgramCounter().increment();
			return;
		}

		registerMachine.getProgramCounter().setCounter(programCounterValue);
	}
}
