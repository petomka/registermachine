package me.petomka.registermachine.model;

/**
 * Represents an exception in the register machine.
 */
public class MachineException extends RuntimeException {

	public MachineException(String msg) {
		super(msg);
	}

}
