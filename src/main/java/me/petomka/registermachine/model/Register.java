package me.petomka.registermachine.model;

public class Register {

	private int value;

	/**
	 * Sets this register's value to the given value, if the given value is greater than or equal to 0.
	 * @param value The register's new value
	 * @throws MachineException if the value is less than 0
	 */
	public void setValue(int value) throws MachineException {
		if(value < 0) {
			throw new MachineException("Register Content cannot be less than zero!");
		}
		this.value = value;
	}

	/**
	 * Gets this registers value.
	 * @return the value stored in the register.
	 */
	public int getValue() {
		return value;
	}

}
