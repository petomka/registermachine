package me.petomka.registermachine.model;

import java.util.HashMap;
import java.util.Map;

public class Memory {

	private Map<Integer, Register> knownRegisters = new HashMap<>();

	/**
	 * Gets a register from the memory. Index must be greater than 0.
	 *
	 * @param index the index of the register
	 * @return the memory register
	 * @throws MachineException if invalid index is given
	 */
	public Register getRegister(int index) throws MachineException {
		if (index < 1) {
			throw new MachineException("Register with index " + index + " does not exist in memory");
		}
		return knownRegisters.computeIfAbsent(index, integer -> new Register());
	}

	/**
	 * Removes any saved registers from the Map if their value is equal to zero.
	 */
	public void cleanUp() {
		knownRegisters.values().removeIf(register -> register.getValue() == 0);
	}

}
