package me.petomka.registermachine.model;

import lombok.Data;
import me.petomka.registermachine.model.commands.Command;

import java.util.ArrayList;
import java.util.List;

@Data
public class Program {

	private List<Command> commands = new ArrayList<>();

	public Command getCommand(int lineNumber) {
		return commands.get(lineNumber - 1);
	}

	public void wipe() {
		commands.clear();
	}

	public void addCommand(Command command) {
		commands.add(command);
	}


}
