package me.petomka.registermachine.model;

import lombok.Getter;
import me.petomka.registermachine.model.commands.Command;
import me.petomka.registermachine.model.commands.CommandSet;

@Getter
public class RegisterMachine {

	private boolean terminated = false;

	private ProgramCounter programCounter = new ProgramCounter();

	private Register accumulator = new Register();

	private Memory memory = new Memory();

	private Program program = new Program();

	private CommandSet commandSet = new CommandSet();

	public void end() {
		terminated = true;
	}

	public void compileProgram(String[] lines) throws MachineException {
		program.wipe();

		int lineNo = 1;

		for (String line : lines) {
			try {
				Command command = commandSet.loadCommand(line);
				program.addCommand(command);
				lineNo++;
			} catch (MachineException exc) {
				throw new MachineException("Error compiling program in line " + lineNo + ": \n" + exc.getMessage());
			}
		}
	}

	public void step() {
		Command command = program.getCommand(programCounter.getValue());
		command.execute(this);
	}

	public String memDump(int registerCount) {
		StringBuilder sb = new StringBuilder();

		sb.append("c(0) = ");
		sb.append(accumulator.getValue());
		sb.append('\n');

		for (int i = 1; i < registerCount; i++) {
			sb.append("c(");
			sb.append(i);
			sb.append(") = ");

			Register register = memory.getRegister(i);
			sb.append(register.getValue());
			sb.append('\n');
		}
		return sb.toString();
	}

}
