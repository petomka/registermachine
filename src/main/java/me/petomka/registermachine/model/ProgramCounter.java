package me.petomka.registermachine.model;

import lombok.Getter;

public class ProgramCounter {

	@Getter
	private int value = 1;

	/**
	 * Sets the program counter to the given value b which must be greater than or equal to 0.
	 * @param b the new program counter
	 * @throws MachineException if b is less than 0
	 */
	public void setCounter(int b) throws MachineException {
		if(b < 0) {
			throw new MachineException("Program Counter cannot be less than zero");
		}
		this.value = b;
	}

	/**
	 * Increments the program counter by one.
	 */
	public void increment() {
		value++;
	}

}
