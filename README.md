# RegisterMachine

Simulates a register machine.

To create a register machine, use ``new RegisterMachine()``.

With this machine, you can then compile a program with ``RegisterMachine#compileProgram(String[])``.
Each array member represents a single command. Every programm must end with the ``end`` command.

Memory is as infinite as it gets in the JVM. The memory is split into registers 
(hence the name, duh). Register range from ``c(1)`` to infinity (disclaimer: your memory on 
your machine may **not** be infinite!). Register ``c(0)`` is called the *accumulator* and not
addressable by commands directly (in this implementation anyways).

**Important:** Line numbers start counting at ``1``!

If your program fails to compile, you are given the line number and a reason why 
it failed. Good luck ¯\\\_(ツ)_/¯

## Commands

``LOAD i`` Loads from the given register index (>0) into the accumulator. <br>
``STORE i`` Stores the accumulator content into the given register (>0). <br>
``ADD i`` Adds the contents of register `i` to the accumulator. <br>
``SUB i`` Subtracts the contents of register `i` from the accumulator, 
but accumulator will be at least 0: `c(0)=:max(0,acc-c(0))` <br>
``MULT i`` Multiplies the accumulator with the contents of register `i`. <br>
``DIV i`` Divides the accumulator by the contents of register `i` and floors the result.<br>
``GOTO i`` Sets the program counter to the given value `i`, must be >0. <br>
``IF i?j GOTO k``, `?<-{=,<=,>=,<,>}` If the condition specified by `i?j` is satisfied, 
the program counter is set to `k`. <br>
``END`` Terminates the machine.

##### Commands with constant values
``C-LOAD l`` Sets the accumulator to `l`. <br>
``C-ADD l`` Adds `l` to the accumulator. <br>
``C-SUB l`` Subtracts `l` from the accumulator. See `SUB` above! <br>
``C-MULT l`` Multiplies the accumulator with `l`. <br>
``C-DIV l`` Divides the accumulator by `l` and floors the result. <br>

##### Commands with indirect access
``IND-LOAD i`` `c(0):=c(c(i))` <br>
``IND-STORE i`` `c(c(i)):=c(0)` <br>
``IND-ADD i`` `c(0):=c(0)+c(c(i))` <br>
``IND-SUB i`` `c(0):=max(0,c(0)-c(c(i)))` <br>
``IND-MULT i`` `c(0):=c(0)*c(c(i))` <br>
``IND-DIV i`` `c(0):=floorDiv(c(0),c(c(i)))`